

# DKT roboto

An environnement to generate custom webfonts and tailored CSS files adapted to the Decathlon ecommerce activity.

The build task will generate tailored CSS and webfonts

----

0. Entry : TTF files containing all latin, latin-ext and cyrillic
1. subset TTF files to each unicode block => latin.tff, latin-ext.ttf, cyrillic.ttf
2. convert and optimize theses files to eot, woff and woff2
3. generate corresponding CSS with unicode-range option

----

##

Liste des pays / encoding :

Latin :
English
French
Spanish
German
Vietnamese

Latin-1 supplement :
French
German
Spanish
Icelandic
Vietnamese

Latin ext-B :
Africa alphabet
Pan-Nigerian
Americanist
Khoisan
Pinyin
Romanian

Latin ext-A :
Latin
Czech
Dutch
Polish
Turkish


Be => latin
De => latin
Es => latin
Fr => latin
Hu => latin-ext
It => latin
Nl => latin-ext
Pl
Pt
Ru => cyrillic / cyrillic-ext
Se => latin-ext
Tr => latin-ext
Uk => latin

Character categories : (as in Google web fonts)

greek
greek-ext
latin
latin-ext
vietnamese
cyrillic-ext
cyrillic

## How to use :

git clone this repo somewhere on your machine
npm install
npm run

## Links / todo / inspiration :

http://design.decathlon.com  

https://bitbucket.org/philip/font-optimizer/src  
https://github.com/dolsup/fontler  
https://www.npmjs.com/package/fontkit  
