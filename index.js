var fontkit = require('fontkit');

// open a font synchronously
var font = fontkit.openSync('font.ttf');

// layout a string, using default shaping features.
// returns a GlyphRun, describing glyphs and positions.
var run = font.layout('hello world!');

// get an SVG path for a glyph
var svg = run.glyphs[0].path.toSVG();

// create a font subset
var subset = font.createSubset();
run.glyphs.forEach(function(glyph) {
  subset.includeGlyph(glyph);
});

subset.encodeStream()
      .pipe(fs.createWriteStream('subset.ttf'));
