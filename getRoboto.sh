#!/bin/sh
rm -rf ./ggl-ttf
rm -rf ./src_ttf
echo "getting roboto source files..."
curl -LOk "https://github.com/google/roboto/releases/download/v2.136/roboto-unhinted.zip"
unzip -j roboto-unhinted.zip -d ggl-ttf
mkdir ./src_ttf
echo "copying TTF files into ./src_ttf..."
cp ./ggl-ttf/*.ttf ./src_ttf
echo "cleaning things..."
rm roboto-unhinted.zip
rm -rf ./ggl-ttf
