const gulp = require("gulp");
const fontmin = require("gulp-fontmin");
const ttf2woff2 = require("gulp-ttf2woff2");
const gutil = require("gulp-util");
const subsets = require("./subsets")();

gulp.task("generate-fonts", function() {
  return Object.keys(subsets).map(function(folder) {
    return gulp
      .src("src_ttf/**/*.ttf")
      .pipe(
        fontmin({
          text: subsets[folder]
        })
      )
      .on('end', function(){ gutil.log(gutil.colors.white.bgBlue.bold(folder),'subsetting done'); })
      .pipe(ttf2woff2())
      .on('end', function(){ gutil.log(gutil.colors.white.bgBlue.bold(folder),'woff2 generation done');  })
      .pipe(gulp.dest("dist/fonts/" + folder));
  });
});
